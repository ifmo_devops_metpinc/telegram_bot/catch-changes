using System.Net.Http.Headers;
using System.Text.Json;

namespace catch_changes_frontend;

public static class RequestBuilder
{
    public static async Task<HttpResponseMessage> GetAsync(string url)
    {
        return await new HttpClient().GetAsync(url);
    }
    
    public static async Task<HttpResponseMessage> PostAsync<T>(string uri, T httpContentModel)
    {
        var content = new StringContent("");
        if (httpContentModel != null)
        {
            var jsonString = JsonSerializer.Serialize(httpContentModel);
            content = new StringContent(jsonString);
            content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
        }

        return await new HttpClient().PostAsync(uri, content);
    }
}