namespace catch_changes_frontend.Models;

public class Table
{
    public Table()
    {
        Id = "";
        Name = "";
        Status = "";
    }
    public Table(string id, string name, string status)
    {
        Id = id;
        Name = name;
        Status = status;
    }

    public string Id { get; set; }
    public string Name { get; set; }
    public string Status { get; set; }

    public override string ToString() => $"Table model. ID: {Id}. Name: {Name}. Status: {Status}";
}