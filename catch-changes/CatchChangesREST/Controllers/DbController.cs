using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using NLog;

namespace CatchChangesREST.Controllers;

[Route("db")]
public class DbController
{
    private readonly Logger _logger = LogManager.GetCurrentClassLogger();
    private readonly SourceContext _sourceContext;

    public DbController(SourceContext sourceContext)
    {
        _sourceContext = sourceContext;
    }

    [Route("tables")]
    [HttpGet]
    public async Task<ActionResult<IEnumerable<TableModel>>> GetTables()
    {
        try
        {
            var tables = _sourceContext.Tables.ToList();
            _logger.Trace($"Received tables from DB. Count: {tables.Count}");
            return new ActionResult<IEnumerable<TableModel>>(tables);
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }

        return new EmptyResult();
    }

    [Route("table")]
    [HttpPost]
    public async Task<IActionResult> AddTable(string id, string name, string status)
    {
        try
        {
            var table = new TableModel {Id = id, Name = name, Status = status};
            await _sourceContext.Tables.AddAsync(table);
            await _sourceContext.SaveChangesAsync();
            _logger.Trace($"Added a new table to DB. Id: {table.Id}. Name: {table.Name}. Status: {table.Status}");
            return new ContentResult
            {
                Content = $"Added a new table to DB. Id: {table.Id}. Name: {table.Name}. Status: {table.Status}",
                StatusCode = 200
            };
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }

        return new ContentResult
        {
            Content = "Error occured while attempting to add data to DB",
            StatusCode = 500
        };
    }
}